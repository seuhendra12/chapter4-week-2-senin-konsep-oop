// Import class
const Person = require('./person.js')

// Person = class parent
class Police extends Person {

	// Mengambil sifat dari class parentnya
	constructor (name, age, city, status){
		super(name, age, city)
		this.status = status
	}
	info(){
		console.log(this.greet() + this.name + ` dari kota ` + this.city)
		console.log(`Polisi ini statusnya ${this.status}`)
	}
}

const police = new Police('Seu', 20, 'Batam', 'Aktif')
police.info()