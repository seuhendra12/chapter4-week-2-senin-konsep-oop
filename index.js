// Objek
const user1 = {
	name : "Seuhendra Setiawan",
	age : 20
}
const user2 = {
	name : "Dinniatul Aqidah Azmi",
	age : 19
}
const user3 = {
	name : "Naufal Asyraf Idrisa",
	age : 20
}
console.log(user2.name);

// // Import class User
// const User = require('./user.js')

// // Instance
// const user = new User('Seu', 20, 'Karimun')

// // Pemanggilan method di class User
// user.info();