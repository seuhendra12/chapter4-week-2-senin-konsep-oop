// Import class
const Person = require('./person.js')

// Instance
const user1 = new Person('Seu', 20, 'Karimun')

// Manggil instancenya
user1.info();