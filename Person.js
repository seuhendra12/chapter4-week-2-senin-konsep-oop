// Pembuatan class user, untuk nama class harus menggunakan uppercase di awal
class Person {

	// Pembuatan Constructor(Argument/Parameter)
	constructor(name, age, city) {

		// Penambahan property, valuenya
		this.name = name
		this.age = age
		this.city = city
	}

	// Pembuatan Method dengan nama method greet
	greet() {
		// console.log(`Hello ${this.name} !`);
		return "Hello "
	}

	// Contoh method manggil method
	info() {
		console.log(this.greet() + this.name + ` dari kota ` + this.city)
	}
}

// Export class User
module.exports = Person;